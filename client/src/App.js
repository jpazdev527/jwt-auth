import React, { useState, useEffect } from "react";
import { Router, navigate } from "@reach/router";

import Navigation from "./components/navigation";
import Login from "./components/login";
import Register from "./components/register";
import Content from "./components/content";
import Protected from "./components/protected";

export const UserContext = React.createContext([]);
function App() {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  const logOutCallback = async () => {
    await fetch('http://localhost:4000/logout', {
      method: 'POST',
      credentials: 'include',
    });
    // Clear user from context
    setUser({});
    navigate('/');
  };

  // Get a new access token if a refresh token exists
  useEffect(() => {
    async function checkRefreshToken() {
      const result = await (await fetch('http://localhost:4000/refresh_token', {
        method: 'POST',
        credentials: 'include',// Needed to include the cookie
        headers: {
          'Content-Type': 'application/json',
        }
      })).json();
      setUser({
        accessToken: result.accessToken
      });
      setLoading(false);
    }
    checkRefreshToken();
  }, []);

  if(loading){
    console.log('Loading');
    return <div>Loading...</div>;
  } 

  return (
    <UserContext.Provider value={[user, setUser]}>
      <div className="App">
        <Navigation logOutCallback={logOutCallback} />
        <Router id="router">
          <Login path="login" />
          <Register path="register" />
          <Protected path="protected" />
          <Content path="/" />
        </Router>
      </div>
    </UserContext.Provider>
  );
}

export default App;
