exports.fakeDB = [
  {
    id: 0,
    email: 'jorge.paz@laureate.net',
    password: '$2a$10$0lrbHIJ5W87kbc3/xg1aOOuDgTFj.2QcN80jJ0NIlp5HOLO/OJIxG' // password test
  }
]