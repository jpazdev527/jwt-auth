require("dotenv/config");
const express = require("express");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const { verify } = require("jsonwebtoken");
const { hash, compare } = require("bcryptjs");
const { fakeDB } = require("./fakeDB");
const {
  createAccessToken,
  createRefreshToken,
  sendTokens
} = require("./tokens");
const { isAuth } = require("./isAuth");

// 1. Register a user
// 2. Login a user
// 3. Logouat a user
// 4. Setup a protected route
// 5. Get a new access token with a refresh token.

const server = express();

// Use express middleware for easier cookie handling.
server.use(cookieParser());

server.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true
  })
);

// Needed to be able to read body data.
server.use(express.json()); // Support JSON encoded bodies.
server.use(express.urlencoded({ extended: true })); // support URL encoded bodies.

// 1. Register a user
server.post("/register", async (req, res) => {
  const { email, password } = req.body;

  try {
    //1. Check if user exists
    const user = fakeDB.find(user => user.email === email);
    if (user) throw new Error("User already exists");
    // 2. If it's a new user, hash the password.
    const hashedPassword = await hash(password, 10);
    // 3. Insert the user in "DB".
    fakeDB.push({
      id: fakeDB.length,
      email,
      password: hashedPassword
    });
    console.log("New user registered: ", fakeDB);
    res.status(200).json({ message: "User created" });
  } catch (error) {
    console.log(
      "An error ocurred when attempting to register a new user",
      error
    );
    res
      .status(400)
      .json({
        error: error.message,
        message: "An error ocurred when attempting to register a new user"
      });
  }
});

// 2. Login a user.
server.post("/login", async (req, res) => {
  const { email, password } = req.body;

  try {
    // 1. Find user in "DB". If user doesn't exist send error.
    const user = fakeDB.find(user => user.email === email);
    if (!user) throw new Error("User does not exist.");
    // 2. Compare crypted password. Send error if not match is found.
    const valid = await compare(password, user.password);
    if (!valid) throw new Error("Password incorrect");
    // 3. Create refresh & accesstoken
    const accessToken = createAccessToken(user.id);
    const refreshToken = createRefreshToken(user.id);
    // 4. Put the refreshtoken in the "database".
    user.refreshToken = refreshToken;
    console.log("Added tokens to DB.", fakeDB);
    // 5. Send token. Refreshtoken as a cookie and accesstoken as a regular response.
    sendTokens(req, res, accessToken, refreshToken);
    console.log(`${email} logged in successfully.`);
  } catch (error) {
    console.error(`Failed login attempt for user: ${email}`);
    res
      .status("401")
      .json({
        error: error.message,
        message: `Failed login attempt for user: ${email}`
      });
  }
});

// 3. logout user.
server.post("/logout", (req, res) => {
  res.clearCookie("refreshtoken", {path: '/refresh_token'});
  res.status(200).json("Logged out");
});

//4. Protected route
server.post("/protected", async (req, res) => {
  try {
    const userId = isAuth(req);
    if (userId !== null) {
      res.status("200").json("This is protected data.");
    }
  } catch (error) {
    console.error('An error ocurred when accessing the protected route.',error);
    res
      .status(401)
      .json({
        error: error.message,
        message: "An error ocurred when accessing the protected route."
      });
  }
});

//5. Get a new access token with a refresh token.
server.post("/refresh_token", (req, res) => {
  const token = req.cookies.refreshtoken;
  // if we don't have a token in our request.
  if (!token) return res.status("401").json({ accessToken: "", message: 'Invalid refresh token' });
  // We have a token, let's verify it!
  let payload = null;
  try {
    payload = verify(token, process.env.REFRESH_TOKEN_SECRET);
  } catch (error) {
    res.status("401").json({ accessToken: "", message: 'Failed validating the payload' });
  }
  try {
    // Token is valid, check if user exists.
    const user = fakeDB.find(user => user.id === payload.userId);
    if (!user) throw new Error("User not found.");
    // User exists, check if refreshtoken exists on user.
    if (user.refreshToken !== token) throw new Error("Invalid refresh token");
    // Token exists, create new refresh and accesstoken.
    const accessToken = createAccessToken(user.id);
    const refreshToken = createRefreshToken(user.id);
    user.refreshToken = refreshToken;
    sendTokens(req, res, accessToken, refreshToken);
  } catch (error) {
    res.status("401").json({ accessToken: "", error: error.message });
  }
});

server.listen(process.env.PORT, () => {
  console.log(`Server listening on port ${process.env.PORT}`);
});
