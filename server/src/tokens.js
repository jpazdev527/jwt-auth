const { sign } = require('jsonwebtoken');

const createAccessToken = (userId) => {
  return sign({userId}, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: '15m',
  });
};

const createRefreshToken = (userId) => {
  return sign({userId}, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: '7d',
  });
};

const sendTokens = (req, res, accessToken, refreshToken) => {
  res.cookie('refreshtoken', refreshToken, {
    httpOnly: true,
    path: '/refresh_token',
  });

  res.status(200).json({
    accessToken,
    email: req.body.email,
  });
};


module.exports = {
  createAccessToken,
  createRefreshToken,
  sendTokens,
}